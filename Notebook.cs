﻿using System;
using System.Collections.Generic;

namespace NotebookApp
{
    public class Notebook
    {
        private Dictionary<int, Note> notes = new Dictionary<int, Note>();
        public bool IsEmpty()
        {
            if (notes.Count == 0)
            {
                return true;
            }
            return false;
        }
        public bool IsInNotes(int id)
        {
            if (notes.ContainsKey(id))
            {
                return true;
            }
            return false;
        }
        //Returns id of the added note
        public int AddNote(string lastName, string firstName, string country, long phoneNumber)
        {
            Note note = new Note(lastName, firstName, country, phoneNumber);
            notes.Add(note.Id, note);
            return note.Id;
        }
        public void EditNote(int id, EditOptions option, string s)
        {
            if (IsInNotes(id))
            {
                switch (option)
                {
                    case EditOptions.FirstName:
                        notes[id].EditFirstName(s);
                        break;
                    case EditOptions.LastName:
                        notes[id].EditLastName(s);
                        break;
                    case EditOptions.MiddleName:
                        notes[id].EditMiddleName(s);
                        break;
                    case EditOptions.Country:
                        notes[id].EditCountry(s);
                        break;
                    case EditOptions.Organization:
                        notes[id].EditOrganization(s);
                        break;
                    case EditOptions.Job:
                        notes[id].EditJob(s);
                        break;
                    case EditOptions.OtherInfo:
                        notes[id].EditOtherInfo(s);
                        break;
                    case EditOptions.Birthday:
                        throw new NotebookException($"This option {option} requires DateTime type");
                    case EditOptions.PhoneNumber:
                        throw new NotebookException($"This option {option} requires long type");
                }
            }
            else
            {
                throw new NotebookException($"No such note with id: {id} in the notebook!");
            }
        }
        public void EditNote(int id, EditOptions option, long phoneNumber)
        {
            if (IsInNotes(id))
            {
                switch (option)
                {
                    case EditOptions.Birthday:
                        throw new NotebookException($"This option {option} requires DateTime type");
                    case EditOptions.PhoneNumber:
                        notes[id].EditPhoneNumber(phoneNumber);
                        break;
                    default:
                        throw new NotebookException($"This option {option} requires string type");
                }
            }
            else
            {
                throw new NotebookException($"No such note with id: {id} in the notebook!");
            }
        }
        public void EditNote(int id, EditOptions option, DateTime birthday)
        {
            if (IsInNotes(id))
            {
                switch (option)
                {
                    case EditOptions.Birthday:
                        notes[id].EditBirthday(birthday);
                        break;
                    case EditOptions.PhoneNumber:
                        throw new NotebookException($"This option {option} requires long type");
                    default:
                        throw new NotebookException($"This option {option} requires string type");
                }
            }
            else
            {
                throw new NotebookException($"No such note with id: {id} in the notebook!");
            }
        }
        public void EditNote(int id, EditOptions option)
        {
            if (IsInNotes(id))
            {
                if (option == EditOptions.OtherInfo)
                {
                    notes[id].ClearOtherInfo();
                }
                else
                {
                    throw new NotebookException($"Bad EditOption passed. Expected {EditOptions.OtherInfo}, got {option}");
                }
            }
            else
            {
                throw new NotebookException($"No such note with id: {id} in the notebook!");
            }
        }
        public void DeleteNote(int id)
        {
            if (IsInNotes(id))
            {
                notes.Remove(id);
            }
            else
            {
                throw new NotebookException($"No such note with id: {id} in the notebook!");
            }
        }
        public void ReadNote(int id)
        {
            if (IsInNotes(id))
            {
                notes[id].ReadNote();
            }
            else
            {
                throw new NotebookException($"No such note with id: {id} in the notebook!");
            }
        }
        public void ShowAllNotes()
        {
            if (IsEmpty())
            {
                Console.WriteLine("The notebook is empty!");
            }
            else
            {
                Console.WriteLine("The notebook contents:");
                foreach (var item in notes)
                {
                    Console.WriteLine(item.Value);
                }
            }
        }
    }
    public enum EditOptions
    {
        FirstName,
        LastName,
        MiddleName,
        PhoneNumber,
        Country,
        Birthday,
        Organization,
        Job,
        OtherInfo
    }
    public class NotebookException : Exception
    {
        private string msg;
        public NotebookException(string msg)
        {
            this.msg = msg;
        }
        public override string Message => this.msg;
    }
}
