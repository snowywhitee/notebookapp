﻿using System;
using System.Linq;

namespace NotebookApp
{
    public class Note
    {
        private int id;
        public int Id { get => this.id; }
        private static int idCount = 0;

        //Requireed values
        private RequiredValues requiredValues;
        public string FirstName { get => requiredValues.FirstName; }
        public string LastName { get => requiredValues.LastName; }
        public string Country { get => requiredValues.Country; }
        public long PhoneNumber { get => requiredValues.PhoneNumber; }
        
        //Optional values
        private string middleName;
        private DateTime birthday;
        private string organization;
        private string job;
        private string otherInfo = "";
        public string OtherInfo { get => otherInfo; }
        public string MiddleName { get => middleName; }
        public DateTime Birthday { get => birthday; }
        public string Organization { get => organization; }
        public string Job { get => job; }

        public Note(string lastName, string firstName, string country, long phoneNumber)
        {
            this.id = Note.idCount;
            Note.idCount++;
            this.requiredValues = new RequiredValues(lastName, firstName, country, phoneNumber);
        }
        public void EditFirstName(string firstName)
        {
            if (firstName.Any(char.IsDigit))
            {
                throw new NotebookException($"First name can't have digits! Problem: {firstName}");
            }
            if (firstName.Any(char.IsSymbol))
            {
                throw new NotebookException($"First name can't have symbols! Problem: {firstName}");
            }
            this.requiredValues.FirstName = firstName;
        }
        public void EditLastName(string lastName)
        {
            if (lastName.Any(char.IsDigit))
            {
                throw new NotebookException($"Last name can't have digits! Problem: {lastName}");
            }
            if (lastName.Any(char.IsSymbol))
            {
                throw new NotebookException($"Last name can't have symbols! Problem: {lastName}");
            }
            this.requiredValues.LastName = lastName;
        }
        public void EditCountry(string country)
        {
            if (country.Any(char.IsDigit))
            {
                throw new NotebookException($"Country name can't have digits! Problem: {country}");
            }
            if (country.Any(char.IsSymbol))
            {
                throw new NotebookException($"Country name can't have symbols! Problem: {country}");
            }
            this.requiredValues.Country = country;
        }
        public void EditPhoneNumber(long phoneNumber)
        {
            if (phoneNumber >= 100000000000 || phoneNumber <= 9999999999)
            {
                throw new NotebookException($"Invalid phone number. Must have 11 digits and be positive. Problem: {phoneNumber}");
            }
            this.requiredValues.PhoneNumber = phoneNumber;
        }
        public void EditMiddleName(string middleName)
        {
            if (middleName.Any(char.IsDigit))
            {
                throw new NotebookException($"Middle name can't have digits! Problem: {middleName}");
            }
            if (middleName.Any(char.IsSymbol))
            {
                throw new NotebookException($"Middle name can't have symbols! Problem: {middleName}");
            }
            this.middleName = Capitalize(middleName);
        }
        public void EditBirthday(DateTime birthday)
        {
            this.birthday = birthday;
        }
        public void EditOrganization(string organization)
        {
            this.organization = Capitalize(organization);
        }
        public void EditJob(string job)
        {
            if (job.Any(char.IsDigit))
            {
                throw new NotebookException($"Job name can't have digits! Problem: {job}");
            }
            if (job.Any(char.IsSymbol))
            {
                throw new NotebookException($"Job name can't have symbols! Problem: {job}");
            }
            this.job = Capitalize(job);
        }
        public void ReadNote()
        {
            Console.WriteLine();
            Console.WriteLine($"Note with id: {id}");
            Console.WriteLine($"\tLast name: {LastName}");
            Console.WriteLine($"\tFirst name: {FirstName}");
            if (middleName != null) Console.WriteLine($"\tMiddle name: {MiddleName}");
            Console.WriteLine($"\tPhone number: {PhoneNumber}");
            Console.WriteLine($"\tCountry: {Country}");
            if (birthday != default(DateTime)) Console.WriteLine($"\tBirthday: {Birthday.ToString("dd.MM.yyyy")}");
            if (organization != null) Console.WriteLine($"\tOrganization: {Organization}");
            if (job != null) Console.WriteLine($"\tJob: {Job}");
            if (otherInfo.Length != 0) Console.WriteLine($"\tOther info: {otherInfo}");
            Console.WriteLine();
        }
        public void EditOtherInfo(string s)
        {
            if (otherInfo == "")
            {
                otherInfo += s;
            }
            else
            {
                otherInfo += "; " + s;
            }
        }
        public void ClearOtherInfo()
        {
            otherInfo = "";
        }
        private class RequiredValues
        {
            private string lastName;
            private string firstName;
            private string country;
            private long phoneNumber;
            public string LastName { get => lastName; set => lastName = value; }
            public string FirstName { get => firstName; set => firstName = value; }
            public string Country { get => country; set => country = value; }
            public long PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
            public RequiredValues(string lastName, string firstName, string country, long phoneNumber)
            {
                //Check if parameters are valid:
                if (lastName.Any(char.IsDigit) || lastName.Any(char.IsSymbol))
                {
                    throw new NotebookException("Last name can't contain digits or symbols.");
                }
                if (firstName.Any(char.IsDigit) || firstName.Any(char.IsSymbol))
                {
                    throw new NotebookException("First name can't contain digits or symbols.");
                }
                if (country.Any(char.IsDigit) || country.Any(char.IsSymbol))
                {
                    throw new NotebookException("Country name can't contain digits or symbols.");
                }
                if (phoneNumber >= 100000000000 || phoneNumber <= 9999999999)
                {
                    throw new NotebookException("Invalid phone number. Must have 11 digits and be positive.");
                }
                this.lastName = Capitalize(lastName);
                this.firstName = Capitalize(firstName);
                this.country = Capitalize(country);
                this.phoneNumber = phoneNumber;
            }
        }
        private static string Capitalize(string s)
        {
            string[] words = s.Split(' ');
            s = "";
            for (int i = 0; i < words.Length; i++)
            {
                if (i == words.Length - 1)
                {
                    s += char.ToUpper(words[i][0]) + words[i].Substring(1).ToLower();
                }
                else
                {
                    s += char.ToUpper(words[i][0]) + words[i].Substring(1).ToLower() + " ";
                }
            }
            return s;
        }
        public override string ToString()
        {
            return $"Note with id: {id}. Last name: {LastName}, First name: {FirstName}, Phone Number: {PhoneNumber}";
        }
    }
}
