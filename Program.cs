﻿using System;

namespace NotebookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            NotebookUserDialog dialog = new NotebookUserDialog();
            dialog.Dialog(new Notebook());

        }
    }
}
