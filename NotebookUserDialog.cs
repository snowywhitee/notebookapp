﻿using System;
using System.Linq;

namespace NotebookApp
{
    public class NotebookUserDialog
    {
        public void AddNote(Notebook notebook)
        {
            string lastName = GetValidString(EditOptions.LastName);
            string firstName = GetValidString(EditOptions.FirstName);
            string country = GetValidString(EditOptions.Country);
            long phoneNumber = GetValidLong();
            int id = notebook.AddNote(lastName, firstName, country, phoneNumber);
            while (true)
            {
                Console.WriteLine("\nWould you like to add other info to your note? Type 'y' for YES, 'n' for NO\n");
                string choice = Console.ReadLine();
                if (choice == "y")
                {
                    EditNote(notebook, id);
                    break;
                }
                else if (choice == "n")
                {
                    Console.Clear();
                    break;
                }
                else
                {
                    ShowWarning("Please type 'y' for YES, 'n' for NO");
                }
            }
        }
        public void EditNote(Notebook notebook)
        {
            if (notebook.IsEmpty())
            {
                ShowWarning("The notebook is empty!");
            }
            else
            {
                EditNote(notebook, GetValidId(notebook));
            }
        }
        public void EditNote(Notebook notebook, int id)
        {
            while (true)
            {
                Console.WriteLine("\nChoose what do you want to edit:");
                Console.WriteLine("\t1 - Last name");
                Console.WriteLine("\t2 - First name");
                Console.WriteLine("\t3 - Middle name");
                Console.WriteLine("\t4 - Phone number");
                Console.WriteLine("\t5 - Country name");
                Console.WriteLine("\t6 - Birthday");
                Console.WriteLine("\t7 - Organization");
                Console.WriteLine("\t8 - Job");
                Console.WriteLine("\t9 - Add other info");
                Console.WriteLine("\t10 - Remove other info");
                Console.WriteLine("\t11 - Don't want to edit anymore\n");
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        notebook.EditNote(id, EditOptions.LastName, GetValidString(EditOptions.LastName));
                        break;
                    case "2":
                        notebook.EditNote(id, EditOptions.FirstName, GetValidString(EditOptions.FirstName));
                        break;
                    case "3":
                        notebook.EditNote(id, EditOptions.MiddleName, GetValidString(EditOptions.MiddleName));
                        break;
                    case "4":
                        notebook.EditNote(id, EditOptions.PhoneNumber, GetValidLong());
                        break;
                    case "5":
                        notebook.EditNote(id, EditOptions.Country, GetValidString(EditOptions.Country));
                        break;
                    case "6":
                        notebook.EditNote(id, EditOptions.Birthday, GetValidDate());
                        break;
                    case "7":
                        notebook.EditNote(id, EditOptions.Organization, GetValidString(EditOptions.Organization));
                        break;
                    case "8":
                        notebook.EditNote(id, EditOptions.Job, GetValidString(EditOptions.Job));
                        break;
                    case "9":
                        notebook.EditNote(id, EditOptions.OtherInfo, GetOtherInfo());
                        break;
                    case "10":
                        notebook.EditNote(id, EditOptions.OtherInfo);
                        break;
                    case "11":
                        goto LoopEnd;
                    default:
                        ShowWarning("Unknown command! Please enter number in [1;11]");
                        continue;
                }
            }
        LoopEnd:
            Console.Clear();
        }
        public string GetOtherInfo()
        {
            string info = "";
            while (true)
            {
                Console.WriteLine("\nPlease enter other info:\n");
                info = Console.ReadLine();
                if (info == "")
                {
                    ShowWarning("This field can't be empty! Try again");
                    continue;
                }
                break;
            }
            return info;
        }
        public string GetValidString(EditOptions option)
        {
            string choice;
            while (true)
            {
                Console.WriteLine($"\nEnter {option}:\n");
                choice = Console.ReadLine();
                if (choice.Any(char.IsDigit))
                {
                    ShowWarning($"{option} can't contain digits. Try again.");
                    continue;
                }
                if (option != EditOptions.Organization)
                {
                    if (choice.Any(char.IsSymbol) || choice.Any(char.IsPunctuation))
                    {
                        ShowWarning($"{option} can't contain symbols. Try again.");
                        continue;
                    }
                }
                if (choice == "")
                {
                    ShowWarning($"{option} can't be empty. Try again");
                    continue;
                }
                break;
            }
            return choice;
        }
        public long GetValidLong()
        {
            long phoneNumber;
            while (true)
            {
                Console.WriteLine("\nEnter the phone number (ex. 87779006030):\n");
                string choice = Console.ReadLine();
                if (!choice.All(char.IsDigit))
                {
                    ShowWarning("Phone number can only contain digits. Try again.");
                    continue;
                }
                else if (choice.Length >= 12 || choice.Length <= 10)
                {
                    ShowWarning("Invalid phone number. Must have 11 digits and be positive. Try again");
                    continue;
                }
                phoneNumber = long.Parse(choice);
                break;
            }
            return phoneNumber;
        }
        public DateTime GetValidDate()
        {
            DateTime date = new DateTime();
            while(true)
            {
                Console.WriteLine("\nPlease enter the date for birthday (ex. 20.11.2000):\n");
                string choice = Console.ReadLine();
                if (! DateTime.TryParse(choice, out date))
                {
                    ShowWarning("Invalid date! Try again");
                    continue;
                }
                break;
            }
            return date;
        }
        public int GetValidId(Notebook notebook)
        {
            string idstr;
            int id;
            while (true)
            {
                Console.WriteLine("\nPlease choose the option:");
                Console.WriteLine("\t1 - I know the note id");
                Console.WriteLine("\t2 - I don't know the note id\n");
                string choice = Console.ReadLine();
                if (choice == "1" || choice == "2")
                {
                    if (choice == "2")
                    {
                        notebook.ShowAllNotes();
                    }
                    while (true)
                    {
                        Console.WriteLine("\nEnter the note id:\n");
                        idstr = Console.ReadLine();
                        if (idstr.All(char.IsDigit))
                        {
                            id = int.Parse(idstr);
                            if (! notebook.IsInNotes(id))
                            {
                                ShowWarning($"There is no such note with the id: {id} in the notebook! Try again");
                                continue;
                            }
                            break;
                        }
                        else
                        {
                            ShowWarning("Id can only contain digits! Try again");
                        }
                    }
                }
                else
                {
                    ShowWarning("Unknown command! Please enter number in [1;2]");
                    continue;
                }
                break;
            }
            return id;
        }
        public void DeleteNote(Notebook notebook)
        {
            if (notebook.IsEmpty())
            {
                ShowWarning("The notebook is empty!");
            }
            else
            {
                notebook.DeleteNote(GetValidId(notebook));
            }
        }
        public void ReadNote(Notebook notebook)
        {
            if (notebook.IsEmpty())
            {
                ShowWarning("The notebook is empty!");
            }
            else
            {
                notebook.ReadNote(GetValidId(notebook));
            }
        }
        public void ShowAllNotes(Notebook notebook)
        {
            if (notebook.IsEmpty())
            {
                ShowWarning("The notebook is empty!");
            }
            else
            {
                notebook.ShowAllNotes();
            }
        }
        public void Dialog(Notebook notebook)
        {
            Console.WriteLine("Welcome to the Notebook app!");
            while (true)
            {
                ShowPrompt();
                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        AddNote(notebook);
                        break;
                    case "2":
                        EditNote(notebook);
                        break;
                    case "3":
                        DeleteNote(notebook);
                        break;
                    case "4":
                        ReadNote(notebook);
                        break;
                    case "5":
                        ShowAllNotes(notebook);
                        break;
                    case "6":
                        Console.Clear();
                        break;
                    case "7":
                        Console.WriteLine("Bye-bye :)");
                        goto LoopEnd;
                    default:
                        ShowWarning("Unknown command! Please enter number in [1;7]");
                        continue;
                }
            }
        LoopEnd:
            Console.WriteLine("Program finished");
        }
        private void ShowPrompt()
        {
            Console.WriteLine("\nChoose one of the options below, type the number:");
            Console.WriteLine("\t1 - Add note to the notebook");
            Console.WriteLine("\t2 - Edit note in the notebook");
            Console.WriteLine("\t3 - Delete note from the notebook");
            Console.WriteLine("\t4 - Read note from the notebook");
            Console.WriteLine("\t5 - Show all notes from the notebook");
            Console.WriteLine("\t6 - Clear the console");
            Console.WriteLine("\t7 - Exit\n");
        }
        private void ShowWarning(string warning)
        {
            Console.WriteLine();
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(warning);
            Console.ResetColor();
            Console.WriteLine();
        }
    }
}
